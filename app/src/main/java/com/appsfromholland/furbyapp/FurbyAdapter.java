package com.appsfromholland.furbyapp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class FurbyAdapter extends ArrayAdapter<Furby> {

    // Constructor
    public FurbyAdapter(Context context, ArrayList<Furby> furbies) {
        super( context, 0, furbies);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        // Furby ophalen
        Furby furby = getItem(position);

        // View aanmaken
        if(convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.furby_listview_item,
                    parent,
                    false
            );
        }

        // Koppelen datasource aan UI
        TextView name = (TextView) convertView.findViewById(R.id.furbyListViewItemNameId);
        ImageView image = (ImageView) convertView.findViewById(R.id.furbyListViewItemImageId);

        // Set furby name
        name.setText( furby.getName() );

        // Get image vanuit /drawable
        int resId = parent.getResources().getIdentifier(
                furby.getImageUrl(),
                "drawable",
                getContext().getPackageName()
        );
        image.setImageResource(resId);

        return convertView;
    }
}
