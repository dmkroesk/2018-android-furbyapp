package com.appsfromholland.furbyapp;

import java.util.ArrayList;

public class FurbyFactory {

    private ArrayList<Furby> furbies = new ArrayList<>();

    // Private constructor, huh?
    private FurbyFactory() {
        generate();
    }

    public ArrayList<Furby> getFurbies() {
        return furbies;
    }

    private static FurbyFactory instance = null;

    // Singleton
    public static FurbyFactory getInstance() {
        if( instance == null ) {
            instance = new FurbyFactory();
        }
        return instance;
    }

    // Generate some furbies
    private void generate() {
        furbies.add( new Furby("Kiwi",
                10,"wee-tah-kah-loo-loo",
                "furby1"));
        furbies.add( new Furby("Sleep",
                8,"wee-tah-kah-wee-loo",
                "furby2"));
        furbies.add( new Furby("Fire Sparke",
                2,"wee-tee-kah-wah-tee",
                "furby3"));
        furbies.add( new Furby("Handy Feet",
                2,"u-nye-loo-lay-doo?",
                "furby4"));
        furbies.add( new Furby("Motor Mouth",
                2,"u-nye-ay-tay-doo?",
                "furby5"));
        furbies.add( new Furby("Baby Cupcake",
                5,"u-nye-boh-doo?",
                "furby6"));
        furbies.add( new Furby("Little Monster",
                5,"u-nye-way-loh-nee-way",
                "furby7"));
        furbies.add( new Furby("Belly Button",
                3,"u-nye-noh-lah",
                "furby8"));
        furbies.add( new Furby("Fuzzy Pumpkin",
                8,"doo?",
                "furby9"));
        furbies.add( new Furby("Moon Tiger",
                7,"doo-dah",
                "furby10"));
    }

}
