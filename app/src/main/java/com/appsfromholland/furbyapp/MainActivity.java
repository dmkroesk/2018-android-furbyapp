package com.appsfromholland.furbyapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    FurbyFactory ff = FurbyFactory.getInstance();
    ListView furbyListView;
    ArrayAdapter furbyAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Create listview
        furbyListView = (ListView) findViewById(R.id.furbyListViewId);

        // Create adapter
//        furbyAdapter = new ArrayAdapter<Furby>(this,
//                android.R.layout.simple_list_item_1,
//                ff.getFurbies());
        furbyAdapter = new FurbyAdapter(this, ff.getFurbies());

        // Koppelen
        furbyListView.setAdapter(furbyAdapter);

        furbyListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.i("TAG_AVANS_24FEB18", "" + position);

                Furby furby = (Furby) ff.getFurbies().get(position);

                Intent intent = new Intent(getApplicationContext(), DetailedActivity.class);
                intent.putExtra("FURBY", furby);
                startActivity(intent);
            }
        });

    }
}
