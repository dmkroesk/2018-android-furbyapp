package com.appsfromholland.furbyapp;

import java.io.Serializable;

public class Furby implements Serializable {

    private String name;
    private int generation;
    private String phrase;
    private String imageUrl;

    public Furby(String name, int generation, String phrase, String imageUrl) {
        this.name = name;
        this.generation = generation;
        this.phrase = phrase;
        this.imageUrl = imageUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getGeneration() {
        return generation;
    }

    public void setGeneration(int generation) {
        this.generation = generation;
    }

    public String getPhrase() {
        return phrase;
    }

    public void setPhrase(String phrase) {
        this.phrase = phrase;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public String toString() {
        return "Furby{" +
                "name='" + name + '\'' +
                ", generation=" + generation +
                ", phrase='" + phrase + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                '}';
    }
}
