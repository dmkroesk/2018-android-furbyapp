package com.appsfromholland.furbyapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailedActivity extends AppCompatActivity {

    TextView name;
    ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed);

        name = (TextView) findViewById(R.id.detailedActivityNameId);
        image = (ImageView) findViewById(R.id.detailedActivityImageId);

        Intent intent = getIntent();
        Furby furby = (Furby) intent.getSerializableExtra("FURBY");

        name.setText( furby.getName().toLowerCase() );

        int resId = this.getResources().getIdentifier(
                furby.getImageUrl(),
                "drawable",
                this.getPackageName());

        image.setImageResource(resId);

    }
}
